import { registerRootComponent } from "expo";
import { NativeBaseProvider, Box, Heading } from "native-base";
import styled from "styled-components/native";

function App() {
	return (
		<NativeBaseProvider>
			<Box flex={1} bg="#fff" alignItems="center" justifyContent="center">
				<Heading size={"lg"}>React Native Base Project</Heading>
			</Box>
		</NativeBaseProvider>
	);
}

export default registerRootComponent(App);
